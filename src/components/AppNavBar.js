// Long method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

// Short method
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'

export default function AppNavBar(){
  return(
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
      {/*
        - className is use instead class, to specify a CSS class

        - changes for Bootstrap 5
        from mr -> to me
        from ml -> to ms
      */}
          <Nav className="ms-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#courses">Courses</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}