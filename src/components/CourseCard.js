import {Row, Col, Card, Button} from 'react-bootstrap'

export default function CourseCard(){
	return(
		        <Row className="my-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>React.js Course</h2>
                        </Card.Title>
                        <Card.Subtitle className = "mt-3">Description: </Card.Subtitle>
                        <Card.Text>
                        	
                            Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
                        </Card.Text>

                        <Card.Subtitle className = "mt-3">Price: </Card.Subtitle>
                        <Card.Text className = "mb-3">
                        	Php 40,000
                        </Card.Text>
                        <Button variant = "primary">Enroll</Button>

                    </Card.Body>
                </Card>
                </Col>
        </Row>
		)
}