import {Container} from 'react-bootstrap'

import './App.css';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';



function App() {
  return (
    <>
    <AppNavBar />
    <Container fluid>
      <Home />
    </Container>
    </>
  );
}

export default App;
